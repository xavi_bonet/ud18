![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)

# UD18 - Conexion JAVA-MYSQL

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| Xavier Bonet Daga | Master | Project Manager | 01/02/2021 |   |   |  |
| David Bonet Daga | Master | Team Member | 01/02/2021 |   |   |  |

#### 2. Description
```
Ejercicios para practicar la conexion JAVA-MYSQL
```

#### 3. Link a un demo con el proyecto desplegado: https://gitlab.com/xavi_bonet/ud18.git

```
UD18 - Conexion JAVA-MYSQL / https://gitlab.com/xavi_bonet/ud18.git
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
Java - jdk-8
IDE - Eclipse Enterprise
Conector (mysql-connector-java) - https://dev.mysql.com/downloads/connector/j/
```
###### Command line 
```

```

#### 5. Screenshot imagen que indique cómo debe verse el proyecto.
