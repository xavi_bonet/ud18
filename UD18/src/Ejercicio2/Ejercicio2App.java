package Ejercicio2;

import ConectionBD.ConnectionDB;

public class Ejercicio2App {

	public static void main(String[] args) {
		
		// Crear objeto conexion
        ConnectionDB connection = new ConnectionDB();
        
        // Crear una base de datos
        connection.createDB("Empleados");
        
        // Crear tablas
        connection.executeQuery("Empleados", "CREATE TABLE Departamentos (codigo INT NOT NULL, nombre nVARCHAR(100), presupuesto INT, PRIMARY KEY (codigo));");
        connection.executeQuery("Empleados", "CREATE TABLE Empleados ( dni varchar(8) NOT NULL, departamento INT NOT NULL, nombre nVARCHAR(100), apellidos nVARCHAR(255), primary key (dni), foreign key(departamento) REFERENCES Departamentos(codigo) ON DELETE cascade ON UPDATE CASCADE );");

        // Insertar valores
        connection.executeQuery("Empleados", "insert into Departamentos values (0,'test1',100)");
        connection.executeQuery("Empleados", "insert into Departamentos values (1,'test2',1000)");
        connection.executeQuery("Empleados", "insert into Departamentos values (2,'test3',10000)");
        connection.executeQuery("Empleados", "insert into Departamentos values (3,'test4',100000)");
        connection.executeQuery("Empleados", "insert into Departamentos values (4,'test5',1000000)");

        connection.executeQuery("Empleados", "insert into Empleados values ('1234567A',0,'test1','test1')");
        connection.executeQuery("Empleados", "insert into Empleados values ('1234567B',1,'test2','test2')");
        connection.executeQuery("Empleados", "insert into Empleados values ('1234567C',2,'test3','test3')");
        connection.executeQuery("Empleados", "insert into Empleados values ('1234567D',3,'test4','test4')");
        connection.executeQuery("Empleados", "insert into Empleados values ('1234567E',4,'test5','test5')");
        
        // Mostar valores
        connection.getValues("Empleados", "select * from Departamentos");
        connection.getValues("Empleados", "select * from Empleados");
        
        // Cerrar conexion
        connection.closeConnection();
        
	}

}
