package Ejercicio4;

import ConectionBD.ConnectionDB;

public class Ejercicio4App {

	public static void main(String[] args) {
		
		// Crear objeto conexion
		ConnectionDB connection = new ConnectionDB();
		
        // Crear una base de datos
        connection.createDB("PeliculasSalas");
        
        // Crear tablas
        connection.executeQuery("PeliculasSalas", "CREATE TABLE Peliculas ( codigo INT AUTO_INCREMENT, nombre NVARCHAR(100), calificacion_edad INT, PRIMARY KEY (codigo) );");
        connection.executeQuery("PeliculasSalas", "CREATE TABLE Salas ( codigo INT AUTO_INCREMENT, nombre NVARCHAR(100), pelicula INT NOT NULL, PRIMARY KEY (codigo), FOREIGN KEY (pelicula) REFERENCES Peliculas(codigo) );");

        // Insertar valores
        connection.executeQuery("PeliculasSalas", "insert into Peliculas (nombre, calificacion_edad) values ('test1',8)");
        connection.executeQuery("PeliculasSalas", "insert into Peliculas (nombre, calificacion_edad) values ('test2',18)");
        connection.executeQuery("PeliculasSalas", "insert into Peliculas (nombre, calificacion_edad) values ('test3',12)");
        connection.executeQuery("PeliculasSalas", "insert into Peliculas (nombre, calificacion_edad) values ('test4',18)");
        connection.executeQuery("PeliculasSalas", "insert into Peliculas (nombre, calificacion_edad) values ('test5',3)");

        connection.executeQuery("PeliculasSalas", "insert into Salas (nombre, pelicula) values ('sala1',5)");
        connection.executeQuery("PeliculasSalas", "insert into Salas (nombre, pelicula) values ('sala2',1)");
        connection.executeQuery("PeliculasSalas", "insert into Salas (nombre, pelicula) values ('sala3',2)");
        connection.executeQuery("PeliculasSalas", "insert into Salas (nombre, pelicula) values ('sala4',3)");
        connection.executeQuery("PeliculasSalas", "insert into Salas (nombre, pelicula) values ('sala5',4)");

        // Mostar valores
        connection.getValues("PeliculasSalas", "select * from Peliculas");
        connection.getValues("PeliculasSalas", "select * from Salas");
        
        // Cerrar conexion
        connection.closeConnection();

	}

}
