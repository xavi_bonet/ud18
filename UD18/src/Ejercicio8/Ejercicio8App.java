package Ejercicio8;

import ConectionBD.ConnectionDB;

public class Ejercicio8App {

	public static void main(String[] args) {

		final String DATABASE_NAME = "GrandesAlmacenes";
		
		// Crear objeto conexion
		ConnectionDB connection = new ConnectionDB();
		
        // Crear una base de datos
        connection.createDB(DATABASE_NAME);
        
        // Crear tablas
        connection.executeQuery(DATABASE_NAME, "CREATE TABLE Productos ( codigo INT auto_increment, nombre NVARCHAR(100), precio int, PRIMARY KEY (codigo) );");
        connection.executeQuery(DATABASE_NAME, "CREATE TABLE Cajeros ( codigo INT auto_increment, nomApels NVARCHAR(255), PRIMARY KEY (codigo) );");
        connection.executeQuery(DATABASE_NAME, "CREATE TABLE Maquinas_registradoras ( codigo INT auto_increment, piso int, PRIMARY KEY (codigo) );");
        connection.executeQuery(DATABASE_NAME, "CREATE TABLE Venta ( cajero INT NOT NULL, maquina INT NOT NULL, producto INT NOT NULL, foreign key(cajero) references Cajeros(codigo) ON delete cascade on update cascade, foreign key(maquina) references Maquinas_registradoras(codigo) ON delete cascade on update cascade, foreign key(producto) references Productos(codigo) ON delete cascade on update cascade );");
        
        // Insertar valores
        connection.executeQuery(DATABASE_NAME, "insert into Productos (nombre, precio) values ('lorem1',100), ('lorem2',200), ('lorem3',300), ('lorem4',400), ('lorem5',500), ('lorem6',600), ('lorem7',700), ('lorem8',800), ('lorem9',900), ('lorem10',1000), ('lorem11',1100);");
        connection.executeQuery(DATABASE_NAME, "insert into Cajeros(nomApels) values ('test'), ('test'), ('test'), ('test'), ('test'), ('test'), ('test'), ('test'), ('test'), ('test');"); 
        connection.executeQuery(DATABASE_NAME, "insert into Maquinas_registradoras(piso) values (1), (1), (1), (1), (1), (1), (1), (1), (1), (1);");
        connection.executeQuery(DATABASE_NAME, "insert into Venta(cajero, maquina, producto) values (1, 1, 1), (2, 2, 2), (3, 3, 3), (4, 4, 4), (5, 5, 5), (6, 6, 6), (7, 7, 7), (8, 8, 8), (9, 9, 9), (10, 10, 10);");

        // Mostar valores
        connection.getValues(DATABASE_NAME, "select * from Productos");
        connection.getValues(DATABASE_NAME, "select * from Cajeros");
        connection.getValues(DATABASE_NAME, "select * from Maquinas_registradoras");
        connection.getValues(DATABASE_NAME, "select * from Venta");
        
        // Cerrar conexion
        connection.closeConnection();
	}
}