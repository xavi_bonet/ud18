package Ejercicio5;

import ConectionBD.ConnectionDB;

public class ejercicio5App {
	
	public static void main(String[] args) {
		final String DB_BAME = "Directores";
		
		ConnectionDB connection = new ConnectionDB();
		
		connection.createDB(DB_BAME);
		connection.executeQuery(DB_BAME, "CREATE TABLE Despachos (numero INT, capacidad INT, PRIMARY KEY (numero));");
		connection.executeQuery(DB_BAME, "CREATE TABLE Directores (dni VARCHAR(8), nom_apels NVARCHAR(255), dni_jefe VARCHAR(8) null, despacho INT NOT NULL, PRIMARY KEY (dni), FOREIGN KEY (dni_jefe) REFERENCES Directores(dni) ON DELETE cascade ON UPDATE CASCADE, FOREIGN KEY (despacho) REFERENCES Despachos(numero) ON DELETE cascade ON UPDATE CASCADE);");
		connection.executeQuery(DB_BAME, "ALTER TABLE Directores CHANGE COLUMN dni_jefe dni_jefe varchar(8) NULL;");
		
		connection.executeQuery(DB_BAME, "insert into Despachos(numero, capacidad) values (0,1), (1,50), (2,10), (3,20), (4,3);");
		connection.executeQuery(DB_BAME, "insert into Directores(dni,nom_apels,dni_jefe,despacho) values ('1234678A','jefecillo',null,0), ('1234568B','jefecillo','1234678A',1), ('1234678C','jefecillo','1234678A',2), ('1234578D','jefecillo','1234678A',3), ('1234578E','jefecillo','1234678A',4);");
		connection.getValues(DB_BAME, "select * from Despachos");
		connection.getValues(DB_BAME, "select * from Directores");
		
		connection.closeConnection();
		
	}

}
