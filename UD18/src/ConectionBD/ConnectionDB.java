package ConectionBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionDB {
	
    private final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private String hostname;
    private String username;
    private String password;
    private String port;
    private Connection conn;
    
    //METODO CONSTRUCTOR
  	public ConnectionDB() {
  		this.hostname = "192.168.1.200";
  		this.username = "remote";
  		this.password = "&BAf7N6&*?TW";
  		this.port = "3306";
  		this.conn = Connection_DB();
  	}
    
	//METODO QUE ABRE LA CONEXION CON SERVER MYSQL
    private Connection Connection_DB() {
        Connection connection = null;
        try {
        	String url = "jdbc:mysql://" + hostname + ":" + port + "?useTimezone=true&serverTimezone=UTC";
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(url, username, password);
            System.out.println("Server Conectado");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Error: " + e);
        }
        return connection;
    }
    
    
    //METODO QUE CIERRA LA CONEXION CON SERVER MYSQL
  	public void closeConnection() {
  		try {
  			conn.close();
  			System.out.print("Server Desconectado");
  			
  		} catch (SQLException ex) {
  			System.out.println(ex.getMessage());
  			
  		}
  	}
  	
  	//METODO QUE CREA LA BASE DE DATOS
  	public void createDB(String name) {
  		try {
  			String Query="CREATE DATABASE "+ name;
  			Statement st= conn.createStatement();
  			st.executeUpdate(Query);
  			System.out.println("DB creada con exito!");
  			
  		}catch(SQLException ex) {
  			System.out.println(ex.getMessage());
  		}	
  	}
  	
  	//METODO QUE EJECUTA UNA QUERY
  	public void executeQuery(String db,String query) {
  		try {
  			String Querydb = "USE "+db+";";
  			Statement stdb= conn.createStatement();
  			stdb.executeUpdate(Querydb);
  			
  			Statement st= conn.createStatement();
  			st.executeUpdate(query);
  			System.out.println("Query ejecutada con exito!");
  			
  		}catch (SQLException ex){
  			System.out.println(ex.getMessage());
  		}
  		
  	}
  	
  	//METODO QUE OBTIENE VALORES MYSQL
  	public void getValues(String db, String query) {
  		try {
  			String Querydb = "USE "+db+";";
  			Statement stdb= conn.createStatement();
  			stdb.executeUpdate(Querydb);
  						
  			Statement st = conn.createStatement();
  			ResultSet resultSet = st.executeQuery(query);
  			
  			ResultSetMetaData rsmd = resultSet.getMetaData();
  			int columnsNumber = rsmd.getColumnCount();
  			while (resultSet.next()) {
  			    for (int i = 1; i <= columnsNumber; i++) {
  			        if (i > 1) System.out.print(",  ");
  			        String columnValue = resultSet.getString(i);
  			        System.out.print(columnValue + " " + rsmd.getColumnName(i));
  			    }
  			    System.out.println("");
  			}
  			
  		} catch (SQLException ex) {
  			System.out.println(ex.getMessage());
  		}
  	
  	}
 
  	
}
