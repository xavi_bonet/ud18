package Ejercicio3;

import ConectionBD.ConnectionDB;

public class ejercicio3App {
	
	public static void main(String[] args) {
		final String DB_BAME = "Almacenes";
		
		
		ConnectionDB connection = new ConnectionDB();
		
		
		connection.createDB(DB_BAME);
		connection.executeQuery(DB_BAME, "CREATE TABLE Almacenes (codigo INT AUTO_INCREMENT, lugar NVARCHAR(100), capacidad INT,PRIMARY KEY (codigo));");
		connection.executeQuery(DB_BAME, "CREATE TABLE Cajas (numReferencia CHAR(5), contenido NVARCHAR(100), valor INT, almacen INT NOT NULL, PRIMARY KEY (numReferencia), FOREIGN KEY (almacen) REFERENCES Almacenes(codigo)ON DELETE cascade ON UPDATE CASCADE);");
		connection.executeQuery(DB_BAME, "insert into Almacenes(lugar, capacidad) values ('berlin',5), ('madrid',5), ('barcelona',5), ('tremp',2), ('amposta',3);");
		connection.executeQuery(DB_BAME, "insert into Cajas(numReferencia,contenido,valor,almacen) values ('aa','clavos',1000,1), ('bb','clavos',1000,2), ('cc','clavos',1000,3), ('dd','clavos',1000,2), ('ee','clavos',1000,1);");
		connection.getValues(DB_BAME, "select * from Almacenes");
		connection.getValues(DB_BAME, "select * from Cajas");
		
		connection.closeConnection();
		
	}

}
